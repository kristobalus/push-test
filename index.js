
var gcm = require('node-gcm');
var config = require('./config');
 
// Set up the sender with your GCM/FCM API key (declare this once for multiple messages)
var sender = new gcm.Sender(config.api_key);
 
// Prepare a message to be sent
// var message = new gcm.Message({
//     data: { 
//         title: 'Title',
//         content: 'Content'
//     }
// });

var message = new gcm.Message({
    // collapseKey: 'demo',
    priority: 'high',
    timeToLive: 0,
    // restrictedPackageName: "kz.mint.otisapp",
    // dryRun: false,
    data: { 
        title: 'Сработал датчик движения',
        content: 'Московское время ' + (new Date().getTime()) + ' мс'
    },
    notification: {
        tag: "object-id",
        title: "Сработал датчик движения",
        icon: "ic_otis_logo_push",
        sound: "push_sound_1.mp3",
        color: "#e65100",
        body: 'Московское время ' + (new Date().getTime()) + ' мс'
    }

});
 
// Specify which registration IDs to deliver the message to
var regTokens = [
    // 'fzWYHuJz_E8:APA91bF2COBY3RKxulhuBPBUMTBhreBPDeNKKqmep6CLVeBV_Go4YGH6PU8Raw10pg2884TC3IZGczJQBVvgtN4UBA8J-FfOkUBlEStjC11a1utmcu9xO3oZ4AmnruKXhPcaavjbFbz3'
    // 'ca4pJKTAVn8:APA91bGdf1HLaDKMZiSdhBaBhFb-qtWsVOVdNoS7-fsQmKj3CMF7ovQ3jbMTeyiXcf0dMhvpHer7rwCqlXfPJ5s2HpQz2f_AwWed2gBEVqQIYfGBNMEMkDpg568IFNlG6UMcSQLR94Nh',
    //'d6VPQckjtO0:APA91bHt_qbu5Q7U1OqYW0YgOm2sI-MCrOxUaq4WyUIAdMi54m42JT8KD6sBwhxUQi96uIxcI3_0u4sLaGcmJ3cRvBOwBHcKs_LvNZUbNHxTz8-9KZkROyNpsBUICygeuOUsI4gwMwOl',
    // 'eYHwMOvE2AM:APA91bH7I8Wvtf9H1KWKopWrqZPTzd0xSgsNmBNp_P5qbWwdd6cnXoveVZ6HYnwD0zo0UdmjVdb0A_aq-Gor9obkPNzvTPoTS6Idy23PZFVNqRUm3xmPRvPnOktUw7JiPN5nMHTUhw_m',
    // 'cFp669EYw0w:APA91bF7HCjD_FA74qgDgL0sVQZADwQRxGcOc1Kjj8gti8Nh4Y_1BzppdNoMlHQpOoeMrUN4oSykGBCLtmByg9lIQn8iv8HYYKN2_5GZsewbsxSHknOzvAIMw_3kwJwTgQ9NYhc0EotN',
    // 'cWvnINqRDAM:APA91bGoP-llnZhhZBnYnq7_szLbdc7GxecMC1KKA03Avf8JDs38eePyQyVhdBd6pg3RTWCwRoMojW3ZHHgKoNNfKbeA8lPTqKlUx6xlzh27Jo_uOMKApGwjcdVfPaAyIJyFOvwBZ0xl',
    // 'eOJhfGY0GX0:APA91bFaari71inMhPN_R9DkHor_E7Jx0HTIEQQE3N600jMZna39r9aPw2Hzf5vMYKAQFni2SQ-zXT8Vdl239osFiHTEaBKbYboDTxYVtiQTHQSX60I-JmXeNvvPKzBYS6cN31nEMrV7',
    // 'cLAzK-rgEbU:APA91bEyYO0Q595ts7LvTrA7J-5F44FUgcLFCdMknr6oTKQ7TQhgzIKu833hhVwm318mVTdV_3PqhhXqziIR3aG4l_AGh_SWqLtNv8fy37P1ojOLJw_Xqn3CnxYqR1hcwQoHKfPGm8Ch',
    // 'deEWUCAVNU4:APA91bEJUQz6Ktvme70Mg_CRFwzuC-WJbl9Gwub6h7m-3yR-RdvzK63rzSZ2e7e2kAs_wvmEWVvyf58Cd3BppopR1KuXYcx8Ug-uSEu3UJ-UX15n6bOh5iAFxsqFlfnA6dUsPCxeB4p2'
    
    // ildar
    // xiaomi
    'dDa05Qkvia0:APA91bGhC8nmvYQypg0GUJflomgXZtnv6X96SiaHvXu_xloADeLwX2JJNejwvVof-ZuC3hLiT1NJF373GdRh14gBrhg-OHgR52qKVQmwzfvF8_0J70yCThcWXkDlk4URTRqAV7FmjWJ9',
    // emulator android 9
    // 'c8iq-4iTTWQ:APA91bE-qi93YthpdbozesdM1m9kmwG_bIi0XB37KRm7aS62fIEq2aNn2vON356KZP_A0IWAXDDxo22BB9s1vQO-rggI8bm4GmIXxi-HOQEhW_w8_7k_sm94Vy-gMt-_52-dz5ouO7D-'
    // dana
    // 'dikZB38KOko:APA91bFIkXR1EfC7-nzQ-ps0JfMzRrcIIM8YRsXzqJG-NEu0mqxa75CzGEmawlzz5fuaiadcNGVLzne9sPY5KxyNLg8JipCHo2AgAOOlhkAw-OelB9b-kfW2eIYyv-XOYosBHrTjzgIF'
];


 
// Actually send the message
sender.send(message, { registrationTokens: regTokens }, function (err, response) {
    if (err) console.error(err);
    else console.log(response);
});
